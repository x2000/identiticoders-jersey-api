FROM openjdk:8

COPY ./target/*.* /usr/app/

WORKDIR /usr/app

RUN sh -c 'touch executable.jar'

ENTRYPOINT ["java","-jar","executable.jar"]  
