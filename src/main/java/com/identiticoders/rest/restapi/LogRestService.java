package com.identiticoders.rest.restapi;

import com.identiticoders.rest.helpers.ResponseBuilderHelper;
import com.identiticoders.rest.database.dao.LogDAO;
import com.identiticoders.rest.database.dao.LogDAOFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;

import com.identiticoders.rest.exception.UserNotFoundException;
import com.identiticoders.rest.filter.AuthenticationFilter;
import com.identiticoders.rest.model.JsonSerializable;

@DeclareRoles({"admin", "user", "guest"})
@Path("/logs")
public class LogRestService extends ResourceConfig {

    @GET
    @PermitAll
    //@RolesAllowed({"admin"}) // only an admin user should be allowed to request all users
    @Produces("application/json")
    public Response getAllLogs(@Context HttpHeaders headers) {
        LogDAO logDao = LogDAOFactory.getLogDAO();

        try {
            List<JsonSerializable> logsJson = new ArrayList<JsonSerializable>();
            logsJson.addAll((Collection<? extends JsonSerializable>) logDao.getAllLogs());

            // Return the users on the response
            return ResponseBuilderHelper.createResponse(Response.Status.OK, logsJson);
        } catch (UserNotFoundException e) {
            return ResponseBuilderHelper.createResponse(Response.Status.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            return ResponseBuilderHelper.createResponse(Response.Status.UNAUTHORIZED);
        }
    }

    private String getId(HttpHeaders headers) {
        // get the email we set in AuthenticationFilter
        List<String> id = headers.getRequestHeader(AuthenticationFilter.HEADER_PROPERTY_ID);

        if (id == null || id.size() != 1) {
            throw new NotAuthorizedException("Unauthorized!");
        }

        return id.get(0);
    }

}
