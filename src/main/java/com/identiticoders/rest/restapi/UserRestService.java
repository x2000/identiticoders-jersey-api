package com.identiticoders.rest.restapi;

import com.identiticoders.rest.database.dao.LogDAO;
import com.identiticoders.rest.database.dao.LogDAOFactory;
import com.identiticoders.rest.helpers.ResponseBuilderHelper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;

import com.identiticoders.rest.database.dao.UserDAO;
import com.identiticoders.rest.database.dao.UserDAOFactory;
import com.identiticoders.rest.database.dao.sqlite.SqlLogDAO;
import com.identiticoders.rest.exception.UserExistingException;
import com.identiticoders.rest.exception.UserNotFoundException;
import com.identiticoders.rest.filter.AuthenticationFilter;
import com.identiticoders.rest.model.JsonSerializable;
import com.identiticoders.rest.model.User;
import com.identiticoders.rest.model.UserSecurity;
import com.identiticoders.rest.security.PasswordSecurity;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import org.apache.log4j.Logger;

@DeclareRoles({"admin", "user", "manager"})
@Path("/users")
public class UserRestService extends ResourceConfig {

    final static Logger logger = Logger.getLogger(UserRestService.class);

    //select all users
    @GET
    //@PermitAll
    @RolesAllowed({"admin", "manager", "user"}) // only an admin user should be allowed to request all users
    @Produces("application/json")
    public Response getAll(@Context HttpHeaders headers) {
        UserDAO userDao = UserDAOFactory.getUserDAO();

        try {
            //get user id sent from jwt
            List<String> userId = headers.getRequestHeader(AuthenticationFilter.HEADER_PROPERTY_ID);

            if (userId == null || userId.size() != 1) {
                return ResponseBuilderHelper.createResponse(Response.Status.UNAUTHORIZED);
            }
            //logged in user => userId.get(0))
            User loggedInUser = userDao.getUser(userId.get(0));

            List<JsonSerializable> usersJson = new ArrayList<JsonSerializable>();
            usersJson.addAll((Collection<? extends JsonSerializable>) userDao.getAllUsers(loggedInUser));

            // Return the users on the response
            return ResponseBuilderHelper.createResponse(Response.Status.OK, usersJson);
        } catch (UserNotFoundException e) {
            return ResponseBuilderHelper.createResponse(Response.Status.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            return ResponseBuilderHelper.createResponse(Response.Status.UNAUTHORIZED);
        }
    }

    //select one user based on id
    @GET
    @Path("{id}")
    @RolesAllowed({"admin", "manager"})
    @Produces("application/json")
    public Response get(@PathParam("id") String id) {
        UserDAO userDao = UserDAOFactory.getUserDAO();

        try {
            // use decoded email from jwt in header
            User user = userDao.getUser(id);

            // Return the user on the response
            return ResponseBuilderHelper.createResponse(Response.Status.OK, user);
        } catch (UserNotFoundException e) {
            return ResponseBuilderHelper.createResponse(Response.Status.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            return ResponseBuilderHelper.createResponse(Response.Status.UNAUTHORIZED);
        }
    }

    //create new user
    @POST
    //@PermitAll
    @RolesAllowed({"admin"})
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUser(@Context HttpHeaders headers,
            UserSecurity userSecurity) {
        UserDAO userDao = UserDAOFactory.getUserDAO();
        LogDAO logDao = LogDAOFactory.getLogDAO();

        try {
            try {
                // check if user no registered already
                userDao.getUserIdByEmail(userSecurity.getEmail());
                throw new UserExistingException(userSecurity.getEmail());
            } catch (UserNotFoundException e) {
                //get user id sent from jwt
                List<String> userId = headers.getRequestHeader(AuthenticationFilter.HEADER_PROPERTY_ID);
                if (userId == null || userId.size() != 1) {
                    return ResponseBuilderHelper.createResponse(Response.Status.UNAUTHORIZED);
                }
                //logged in user => userId.get(0))
                User loggedInUser = userDao.getUser(userId.get(0));

                // standard user role
                userSecurity.setRole("user");
                // store plain password
                String plainPassword = userSecurity.getPassword();
                // generate password
                userSecurity.setPassword(PasswordSecurity.generateHash(userSecurity.getPassword()));
                // create user
                userDao.createUser(userSecurity);
                //insert logs
                String userType = "user".equals(userSecurity.getRole())?"Normal":userSecurity.getRole();
                String message = "Create "+ userType +" User";
                logDao.insertLogs(loggedInUser.getId(),
                        loggedInUser.getname(), loggedInUser.getDepartment(),
                        loggedInUser.getmobileNumber(), message);

                // return
                return ResponseBuilderHelper.createResponse(Response.Status.OK, "User created");
            }
        } catch (UserExistingException e) {
            return ResponseBuilderHelper.createResponse(Response.Status.CONFLICT, e.getMessage());
        } catch (Exception e) {
            return ResponseBuilderHelper.createResponse(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    //update user
    @PUT
    @Path("{id}")
    @RolesAllowed({"admin", "manager", "user"})
    @Produces("application/json")
    public Response update(@PathParam("id") String id,
            @Context HttpHeaders headers, User userToBeUpdated) {

        UserDAO userDao = UserDAOFactory.getUserDAO();
        LogDAO logDao = LogDAOFactory.getLogDAO();

        try {
            //get user id sent from jwt
            List<String> userId = headers.getRequestHeader(AuthenticationFilter.HEADER_PROPERTY_ID);
            if (userId == null || userId.size() != 1) {
                return ResponseBuilderHelper.createResponse(Response.Status.UNAUTHORIZED);
            }
            //logged in user => userId.get(0))
            User loggedInUser = userDao.getUser(userId.get(0));

            //apply validation
            if (!updateUserValidated(loggedInUser, userToBeUpdated)) {
                return ResponseBuilderHelper.createResponse(Response.Status.UNAUTHORIZED);
            }

            userToBeUpdated.setId(id);
            userDao.updateUser(userToBeUpdated);

            //insertLogs
            String message = "Update User " + userToBeUpdated.getname();
            logDao.insertLogs(loggedInUser.getId(),
                    loggedInUser.getname(), loggedInUser.getDepartment(),
                    loggedInUser.getmobileNumber(), message);

            // Return the token on the response
            return ResponseBuilderHelper.createResponse(Response.Status.OK, "User updated");
        } catch (UserNotFoundException e) {
            return ResponseBuilderHelper.createResponse(Response.Status.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            return ResponseBuilderHelper.createResponse(Response.Status.UNAUTHORIZED);
        }

    }

    @DELETE
    @Path("{id}")
    @RolesAllowed({"admin", "manager"})
    @Produces("application/json")
    public Response delete(@Context HttpHeaders headers,
            @PathParam("id") String id) {
        UserDAO userDao = UserDAOFactory.getUserDAO();
        LogDAO logDao = LogDAOFactory.getLogDAO();

        try {
            //perform delete user
            userDao.deleteUser(id);

            //find operator userId
            List<String> userId = headers.getRequestHeader(AuthenticationFilter.HEADER_PROPERTY_ID);
            if (userId == null || userId.size() != 1) {
                return ResponseBuilderHelper.createResponse(Response.Status.UNAUTHORIZED);
            }

            //logged in user => userId.get(0))
            User loggedInUser = userDao.getUser(userId.get(0));

            //insertLogs
            String message = "Delete User " + id;
            logDao.insertLogs(loggedInUser.getId(),
                    loggedInUser.getname(), loggedInUser.getDepartment(),
                    loggedInUser.getmobileNumber(), message);

            // Return the response
            return ResponseBuilderHelper.createResponse(Response.Status.OK, "User deleted");
        } catch (UserNotFoundException e) {
            return ResponseBuilderHelper.createResponse(Response.Status.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            return ResponseBuilderHelper.createResponse(Response.Status.UNAUTHORIZED);
        }

    }

    private String getId(HttpHeaders headers) {
        // get the email we set in AuthenticationFilter
        List<String> id = headers.getRequestHeader(AuthenticationFilter.HEADER_PROPERTY_ID);

        if (id == null || id.size() != 1) {
            throw new NotAuthorizedException("Unauthorized!");
        }

        return id.get(0);
    }

    private boolean updateUserValidated(User loggedInUser, User userToBeUpdated) {
        if ("admin".equals(loggedInUser.getRole().toLowerCase())) {
            return true;
        } else if ("manager".equals(loggedInUser.getRole().toLowerCase())
                && loggedInUser.getDepartment() != null
                && userToBeUpdated.getDepartment() != null
                && loggedInUser.getDepartment().equals(userToBeUpdated.getDepartment())) {
            return true;
        } else {
            return ("user".equals(loggedInUser.getRole().toLowerCase())
                    && loggedInUser.getId() != null
                    && userToBeUpdated.getId() != null
                    && loggedInUser.getId().equals(userToBeUpdated.getId()));
        }
    }

}
