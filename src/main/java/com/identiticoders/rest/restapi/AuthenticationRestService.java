package com.identiticoders.rest.restapi;

import com.identiticoders.rest.helpers.ResponseBuilderHelper;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;

import com.identiticoders.rest.database.dao.UserDAO;
import com.identiticoders.rest.database.dao.UserDAOFactory;
import com.identiticoders.rest.exception.UserNotFoundException;
import com.identiticoders.rest.filter.AuthenticationFilter;
import com.identiticoders.rest.model.Credentials;
import com.identiticoders.rest.model.UserSecurity;
import com.identiticoders.rest.security.PasswordSecurity;
import com.identiticoders.rest.security.TokenSecurity;

@DeclareRoles({"admin", "user", "guest"})
@Path("/authenticate")
public class AuthenticationRestService extends ResourceConfig {

    @POST
    @PermitAll
    @Produces("application/json")
    @Consumes("application/json")
    public Response authenticate(Credentials credentials) {
        UserDAO userDao = UserDAOFactory.getUserDAO();

        try {
            String id = userDao.getUserIdByEmail(credentials.getEmail());
            UserSecurity userSecurity = userDao.getUserAuthentication(id);

            if (PasswordSecurity.validatePassword(credentials.getPassword(), userSecurity.getPassword()) == false) {
                return ResponseBuilderHelper.createResponse(Response.Status.UNAUTHORIZED);
            }

            // generate a token for the user
            String token = TokenSecurity.generateJwtToken(id);

            // write the token to the database
            UserSecurity sec = new UserSecurity(null, token);
            sec.setId(id);
            userDao.setUserAuthentication(sec);

            Map<String, Object> map = new HashMap<String, Object>();
            map.put(AuthenticationFilter.AUTHORIZATION_PROPERTY, token);

            // Return the token on the response
            return ResponseBuilderHelper.createResponse(Response.Status.OK, map);
        } catch (UserNotFoundException e) {
            return ResponseBuilderHelper.createResponse(Response.Status.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            return ResponseBuilderHelper.createResponse(Response.Status.UNAUTHORIZED);
        }

    }

}
