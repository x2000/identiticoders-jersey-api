package com.identiticoders.rest.helpers;

import org.glassfish.jersey.server.ResourceConfig;

import com.identiticoders.rest.filter.AuthenticationFilter;

/**
 * set the filter applications manually and not via web.xml
 */
public class RestApplicationConfigHelper extends ResourceConfig {

    public RestApplicationConfigHelper() {
        packages("com.identiticoders.rest.filter");
        register(AuthenticationFilter.class);
    }
}
