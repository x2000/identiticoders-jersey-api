package com.identiticoders.rest.model;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class Log implements JsonSerializable {

    // global unique identifier
    private String id = null;
    private String id_user = null;
    private String name = null;
    private String department = null;
    private String mobileNumber = null;
    private String action = null;

    public Log() {
    }

    public Log(String id, String id_user, String name, String department, String mobileNumber, String action) {
        this.id = id;
        this.id_user = id_user;
        this.name = name;
        this.mobileNumber = mobileNumber;
        this.department = department;
        this.action = action;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public String getmobileNumber() {
        return mobileNumber;
    }

    public void setmobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    @Override
    public String toString() {
        return "User [id=" + id + 
                ", id_user=" + id_user + 
                ", name=" + name + 
                ", department=" + department + 
                ", mobileNumber=" + mobileNumber+ 
                ", action=" + action + "]";
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", this.id);
        jsonObject.put("id_user", this.id_user);
        jsonObject.put("name", this.name);
        jsonObject.put("department", this.department);
        jsonObject.put("mobileNumber", this.mobileNumber);
        jsonObject.put("action", this.action);
        return jsonObject;
    }

}
