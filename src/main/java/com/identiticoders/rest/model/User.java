package com.identiticoders.rest.model;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class User implements JsonSerializable {

    // global unique identifier
    private String id = null;
    private String email = null;
    private String name = null;
    private String mobileNumber = null;
    private String role = null;
    private String department = null;

    public User() {
    }

    public User(String id, String email, String name, String mobileNumber, 
            String role, String department) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.mobileNumber = mobileNumber;
        this.role = role;
        this.department = department;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public String getmobileNumber() {
        return mobileNumber;
    }

    public void setmobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
    
    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    @Override
    public String toString() {
        return "User [id=" + id + 
                ", email=" + email + 
                ", name=" + name + 
                ", mobileNumber=" + mobileNumber+ 
                ", role=" + role + 
                ", department=" + department + "]";
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", this.id);
        jsonObject.put("email", this.email);
        jsonObject.put("name", this.name);
        jsonObject.put("mobileNumber", this.mobileNumber);
        jsonObject.put("role", this.role);
        jsonObject.put("department", this.department);
        return jsonObject;
    }

}
