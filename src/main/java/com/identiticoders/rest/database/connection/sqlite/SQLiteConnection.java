package com.identiticoders.rest.database.connection.sqlite;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteOpenMode;

import com.identiticoders.rest.config.DbConfig;
import com.identiticoders.rest.security.PasswordSecurity;

public class SQLiteConnection implements com.identiticoders.rest.database.connection.Connection {

    final static Logger logger = Logger.getLogger(SQLiteConnection.class);

    Connection connection = null;

    @Override
    public Object get() {
        return connection;
    }

    @Override
    public boolean open() {
        connection = null;
        try {
            Class.forName("org.sqlite.JDBC");

            SQLiteConfig config = new SQLiteConfig();
            config.setOpenMode(SQLiteOpenMode.READWRITE);
            config.setOpenMode(SQLiteOpenMode.CREATE);
            // serialized mode for concurrent access - TODO: use connection pooling
            config.setOpenMode(SQLiteOpenMode.FULLMUTEX);

            String path = null;

            if (DbConfig.getDbPath() != null) {
                path = DbConfig.getDbPath();
            } else if (DbConfig.getDbName() != null) {
                path = "jdbc:sqlite:" + DbConfig.getDbName();
            } else {
                path = "jdbc:sqlite:default-database";
            }

            connection = DriverManager.getConnection(path, config.toProperties());

        } catch (Exception e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        logger.debug("Opened database successfully");

        System.out.println(System.getProperty("catalina.base"));
        
        // seed
        // database openend - init database schema
        createUserSchema();
        createAuditSchema();
        // create user sed
        createAdminUser();
        createManagerUser();
        createNormalUser();

        return true;
    }

    @Override
    public boolean close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                logger.debug("Databank not closed correctly: " + e.getMessage());
            }
            return true;
        }
        return false;
    }

    private boolean checkForUserSchema() {
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='USER';");
            if (rs.next()) {
                return true;
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        }
        return false;
    }

    private void createUserSchema() {
        if (checkForUserSchema() == true) {
            return;
        }

        try {
            Statement stmt = connection.createStatement();
//            String sql = "DROP TABLE USER";
//            stmt.executeUpdate(sql);
//            stmt.close();

            String sql = "CREATE TABLE USER( "
                    + " ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    + " EMAIL 	       TEXT NOT NULL, "
                    + " NAME           TEXT, "
                    + " MOBILENUMBER   TEXT, "
                    + " PASSWORD       TEXT NOT NULL, "
                    + " TOKEN          TEXT, "
                    + " ROLE           TEXT, "
                    + " DEPARTMENT     TEXT)";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (Exception e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    private boolean checkForAuditSchema() {
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='LOGS';");
            if (rs.next()) {
                return true;
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        }
        return false;
    }

    private void createAuditSchema() {
        if (checkForAuditSchema() == true) {
            return;
        }

        try {
            Statement stmt = connection.createStatement();
//            String sql = "DROP TABLE USER";
//            stmt.executeUpdate(sql);
//            stmt.close();

            String sql = "CREATE TABLE LOGS ( "
                    + " ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    + " ID_USER	        TEXT NOT NULL, "
                    + " NAME            TEXT, "
                    + " DEPARTMENT      TEXT, "
                    + " MOBILENUMBER    TEXT, "
                    + " ACTION          TEXT)";
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (Exception e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    private boolean checkForAdminUser() {
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT email FROM USER  WHERE email='admin@gmail.com';");
            if (rs.next()) {
                return true;
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        }
        return false;
    }

    private void createAdminUser() {
        if (checkForAdminUser()) {
            return;
        }

        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(
                    "INSERT INTO USER("
                    + "email,name,mobileNumber,password,role,department) VALUES"
                    + "(?,?,?,?,?,?)");

            stmt.setString(1, "admin@gmail.com");
            stmt.setString(2, "admin");
            stmt.setString(3, "12345");
            stmt.setString(4, PasswordSecurity.generateHash("secret"));
            stmt.setString(5, "admin");
            stmt.setString(6, "general");
            stmt.executeUpdate();
            
            //insert logs
            insertLogs(connection, "-1", "System", "System", "System-1", "Create Admin User");
        } catch (SQLException e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        } catch (InvalidKeySpecException e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.warn(e.getMessage());
            }
        }
    }

    private boolean checkForManagerUser() {
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT email FROM USER  WHERE email='manager@gmail.com';");
            if (rs.next()) {
                return true;
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        }
        return false;
    }

    private void createManagerUser() {
        if (checkForManagerUser()) {
            return;
        }

        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(
                    "INSERT INTO USER("
                    + "email,name,mobileNumber,password,role,department) VALUES"
                    + "(?,?,?,?,?,?)");

            stmt.setString(1, "manager@gmail.com");
            stmt.setString(2, "manager");
            stmt.setString(3, "54321");
            stmt.setString(4, PasswordSecurity.generateHash("secret"));
            stmt.setString(5, "manager");
            stmt.setString(6, "dep1");
            stmt.executeUpdate();
            
            //insert logs
            insertLogs(connection, "-1", "System", "System", "System-1", "Create Manager User");

            //create manager for dep2
            stmt.setString(6, "dep2");
            stmt.executeUpdate();
            
            //insert logs
            insertLogs(connection, "-1", "System", "System", "System-1", "Create Manager User");
        } catch (SQLException e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        } catch (InvalidKeySpecException e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.warn(e.getMessage());
            }
        }
    }

    private boolean checkForNormalUser() {
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT email FROM USER  WHERE email='user@gmail.com';");
            if (rs.next()) {
                return true;
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        }
        return false;
    }

    private void createNormalUser() {
        if (checkForNormalUser()) {
            return;
        }

        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(
                    "INSERT INTO USER("
                    + "email,name,mobileNumber,password,role,department) VALUES"
                    + "(?,?,?,?,?,?)");

            stmt.setString(1, "user@gmail.com");
            stmt.setString(2, "user");
            stmt.setString(3, "123123123");
            stmt.setString(4, PasswordSecurity.generateHash("secret"));
            stmt.setString(5, "user");
            stmt.setString(6, "dep1");
            stmt.executeUpdate();
            //insert logs
            insertLogs(connection, "-1", "System", "System", "System-1", "Create Normal User");

            //create user for dep2
            stmt.setString(6, "dep2");
            stmt.executeUpdate();
            //insert logs
            insertLogs(connection, "-1", "System", "System", "System-1", "Create Normal User");
        } catch (SQLException e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        } catch (InvalidKeySpecException e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.warn(e.getMessage());
            }
        }
    }

    private void insertLogs(Connection conn, String id_user,
            String name, String department, String mobileNumber,
            String action) {
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("INSERT INTO LOGS("
                    + "id_user,name,department,mobileNumber,action) VALUES"
                    + "(?,?,?,?,?)");

            stmt.setString(1, id_user);
            stmt.setString(2, name);
            stmt.setString(3, department);
            stmt.setString(4, mobileNumber);
            stmt.setString(5, action);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.warn(e.getMessage());
            }
        }
    }
}
