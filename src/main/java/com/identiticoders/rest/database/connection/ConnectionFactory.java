package com.identiticoders.rest.database.connection;

import com.identiticoders.rest.database.connection.sqlite.SQLiteConnection;

public class ConnectionFactory {

    private static Connection connection = null;

    public static Connection getConnection() {
        if (connection != null) {
            return connection;
        }

        connection = new SQLiteConnection();

        // open connection
        connection.open();

        return connection;
    }
}
