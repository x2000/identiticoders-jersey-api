package com.identiticoders.rest.database.dao.sqlite;

import com.identiticoders.rest.database.dao.LogDAO;
import com.identiticoders.rest.model.Log;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class SqlLogDAO implements LogDAO {

    final static Logger logger = Logger.getLogger(SqlLogDAO.class);

    private Connection connection = null;

    public SqlLogDAO(com.identiticoders.rest.database.connection.Connection connection) {
        this.connection = (Connection) connection.get();
    }

    @Override
    public List<Log> getAllLogs() {
        logger.debug("getAllLogs");

        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<Log> logs = new ArrayList<Log>();

        try {
            stmt = connection.prepareStatement("SELECT id, id_user, name, department, mobileNumber, action FROM LOGS;");
            rs = stmt.executeQuery();

            while (rs.next()) {
                String id = String.valueOf(rs.getInt("id"));
                String id_user = rs.getString("id_user");
                String name = rs.getString("name");
                String department = rs.getString("department");
                String mobileNumber = rs.getString("mobilenumber");
                String action = rs.getString("action");

                logs.add(new Log(id, id_user, name, department, mobileNumber, action));
            }

        } catch (SQLException e) {
            logger.debug(e.getClass().getName() + ": " + e.getMessage());
        } finally {
            try {
                rs.close();
                stmt.close();
            } catch (SQLException e) {
                logger.warn(e.getMessage());
            }
        }

        return logs;
    }

    @Override
    public void insertLogs(String id_user,
            String name, String department, String mobileNumber,
            String action) throws Exception {
        logger.debug("insertLogs");
        PreparedStatement stmt = null;
        try {
            stmt = this.connection.prepareStatement("INSERT INTO LOGS("
                    + "id_user,name,department,mobileNumber,action) VALUES"
                    + "(?,?,?,?,?)");

            stmt.setString(1, id_user);
            stmt.setString(2, name);
            stmt.setString(3, department);
            stmt.setString(4, mobileNumber);
            stmt.setString(5, action);
            stmt.executeUpdate();
        } catch (Exception ex){
            logger.debug(ex.getClass().getName() + ": " + ex.getMessage());
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.warn(e.getMessage());
            }
        }
    }
}
