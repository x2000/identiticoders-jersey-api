package com.identiticoders.rest.database.dao;

import com.identiticoders.rest.model.Log;
import java.util.List;

public interface LogDAO {

    public List<Log> getAllLogs();

    public void insertLogs(String id_user,
            String name, String department, String mobileNumber,
            String action) throws Exception;
}
