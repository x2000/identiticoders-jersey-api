package com.identiticoders.rest.database.dao;

import java.util.List;

import com.identiticoders.rest.exception.UserExistingException;
import com.identiticoders.rest.exception.UserNotFoundException;
import com.identiticoders.rest.model.User;
import com.identiticoders.rest.model.UserSecurity;

public interface UserDAO {
	public boolean createUser( UserSecurity user ) throws UserExistingException;
	
	public String getUserIdByEmail( String email ) throws UserNotFoundException;
	public User getUser( String id ) throws UserNotFoundException;
	
	public List<User> getAllUsers();
	public List<User> getAllUsers(User loggedInUser);
	
	public UserSecurity getUserAuthentication( String id ) throws UserNotFoundException;
	public boolean setUserAuthentication( UserSecurity user ) throws UserNotFoundException;
	
	public boolean updateUser( User user ) throws UserNotFoundException;
	public boolean deleteUser( String id ) throws UserNotFoundException;
}
