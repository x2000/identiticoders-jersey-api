package com.identiticoders.rest.database.dao;

import com.identiticoders.rest.database.connection.Connection;
import com.identiticoders.rest.database.connection.ConnectionFactory;
import com.identiticoders.rest.database.dao.sqlite.SqlLogDAO;

public class LogDAOFactory {

    public static LogDAO getLogDAO() {
        // get connection
        Connection connection = ConnectionFactory.getConnection();

        return new SqlLogDAO(connection);

    }
}
