package com.identiticoders.rest.database.dao;

import com.identiticoders.rest.database.connection.Connection;
import com.identiticoders.rest.database.connection.ConnectionFactory;
import com.identiticoders.rest.database.dao.sqlite.SqlUserDAO;

public class UserDAOFactory {

    public static UserDAO getUserDAO() {
        // get connection
        Connection connection = ConnectionFactory.getConnection();

        return new SqlUserDAO(connection);

    }
}
